#!/bin/bash

set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
start=$(date +%s.%N)

build(){
    
    if [ "${CPU}" == "aarch64" ]; then
        CPUVARDIR=aarch64le
        CPUVAR=aarch64le
    elif [ "${CPU}" == "armv7" ]; then
        CPUVARDIR=armle-v7
        CPUVAR=armv7le
    elif [ "${CPU}" == "x86_64" ]; then
        CPUVARDIR=x86_64
        CPUVAR=x86_64
    else
        echo "Invalid architecture. Exiting..."
        exit 1
    fi

    echo "CPU set to ${CPUVAR}"
    echo "CPUVARDIR set to ${CPUVARDIR}"
    export CPUVARDIR CPUVAR
    export ARCH=${CPU}

    export PROJECT_INSTALL_BASE=${PWD}/install/${CPUVARDIR}
    
    ROS2_HOST_INSTALL_PATH=$QNX_TARGET/$CPUVARDIR/opt/ros/foxy

    if [ -f "$QNX_TARGET/$CPUVARDIR/opt/ros/foxy/local_setup.bash" ]; then
	      ROS2_HOST_INSTALL_PATH=$QNX_TARGET/$CPUVARDIR/opt/ros/foxy
	      CMAKE_MODULE_PATH="$PWD/platform/Modules"
	      NUMPY_HEADERS=$QNX_TARGET/$CPUVARDIR/usr/lib/python3.8/site-packages/numpy/core/include
        echo "Found ROS2 Installation in $ROS2_HOST_INSTALL_PATH"
    else
	echo "ROS2 not found in $ROS2_HOST_INSTALLATION_PATH"
	echo "Searching in $HOME/ros2_foxy/install/$CPUVARDIR"
	if [ -f "$HOME/ros2_foxy/install/$CPUVARDIR/local_setup.bash" ]; then
	    ROS2_HOST_INSTALL_PATH=$HOME/ros2_foxy/install/$CPUVARDIR
	    CMAKE_MODULE_PATH=" "
	    NUMPY_HEADERS=${ROS2_HOST_INSTALL_PATH}/usr/lib/python3.8/site-packages/numpy/core/include
	    echo "ROS2 found in $ROS2_HOST_INSTALL_PATH"
        else
	    echo "Failed to find ROS2 in expected locations, please set ROS2_HOST_INSTALL_PATH"
	    continue
        fi
    fi
    
    export ROS2_HOST_INSTALL_PATH

    echo "Building AutowareAuto for $CPUVAR architecture. Logging output in logs/colcon_make_$arch.log..."
    # sourcing the ROS base installation setup script for the target architecture
    # to configure the ROS cross-compilation environment
    . $ROS2_HOST_INSTALL_PATH/local_setup.bash

    printenv | grep "ROS"
    printenv | grep "COLCON"

    colcon build --merge-install --cmake-force-configure \
        --build-base=build/${CPUVARDIR} \
        --install-base=${PROJECT_INSTALL_BASE} \
        --cmake-args \
            -DCMAKE_TOOLCHAIN_FILE="${PROJECT_ROOT}/platform/qnx.nto.toolchain.cmake" \
            -DBUILD_TESTING:BOOL="OFF" \
            -DCMAKE_BUILD_TYPE="Release" \
	          -DROS2_HOST_INSTALL_PATH=$ROS2_HOST_INSTALL_PATH \
            -DNUMPY_HEADERS=${NUMPY_HEADERS} \
	          --no-warn-unused-cli

}

if [ ! -d "${QNX_TARGET}" ]; then
    echo "QNX_TARGET is not set. Exiting..."
    exit 1
fi

export PROJECT_ROOT=${PWD}

CPUS=("aarch64" "x86_64")

if [ -z "$CPU" ]; then
    for CPU in ${CPUS[@]}; do
        build
    done
elif [ $CPU == "x86_64" ] || [ $CPU == "armv7" ] || [ $CPU == "aarch64" ] ; then
    build
else
    echo "invalid $CPU please set arch to one of the following x86_64, armv7, or aarch64 or unset arch to build all platforms"
    exit 1
fi

duration=$(echo "$(date +%s.%N) - $start" | bc)
execution_time=`printf "%.2f seconds" $duration`
echo "Build Successful. Build time: $execution_time"
exit 0
