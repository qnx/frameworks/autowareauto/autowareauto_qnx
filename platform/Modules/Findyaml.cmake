find_package(yaml CONFIG QUIET)
if(NOT yaml_FOUND)
  find_path(yaml_INCLUDE_DIR NAMES yaml.h)
  find_library(yaml_LIBRARY NAMES yaml)
  include (FindPackageHandleStandardArgs)
  find_package_handle_standard_args (yaml DEFAULT_MSG
    yaml_LIBRARY
    yaml_INCLUDE_DIR)
  mark_as_advanced (yaml_LIBRARY
    yaml_INCLUDE_DIR)
else()
  if(TARGET yaml)
    set(yaml_LIBRARY yaml)
  elseif(TARGET yaml::yaml)
    set(yaml_LIBRARY yaml::yaml)
  endif()
  list(APPEND yaml_TARGETS ${yaml_LIBRARY})
endif()
