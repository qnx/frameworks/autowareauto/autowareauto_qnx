find_package(console_bridge CONFIG QUIET)
find_path(console_bridge_INCLUDE_DIR console_bridge PATH_SUFFIXES console_bridge)
find_library(console_bridge_LIBRARY NAMES console_bridge)
if(NOT console_bridge_FOUND)
  include (FindPackageHandleStandardArgs)
  find_package_handle_standard_args (console_bridge DEFAULT_MSG
    console_bridge_LIBRARY
    console_bridge_INCLUDE_DIR)
  mark_as_advanced (console_bridge_LIBRARY
    console_bridge_INCLUDE_DIR)
else()
  if(TARGET console_bridge)
    add_library(console_bridge::console_bridge UNKNOWN IMPORTED)
  endif()
  set_target_properties(console_bridge::console_bridge PROPERTIES
          INTERFACE_INCLUDE_DIRECTORIES "${console_bridge_INCLUDE_DIR}")
  set_property(TARGET console_bridge::console_bridge APPEND PROPERTY
          IMPORTED_LOCATION "${console_bridge_LIBRARY}")
endif()
