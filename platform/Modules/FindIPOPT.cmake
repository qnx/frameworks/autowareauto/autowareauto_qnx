find_library(IPOPT_LIBRARIES NAMES ipopt)
set(IPOPT_INCLUDE_DIRS $ENV{QNX_TARGET}/usr/include/coin-or)
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(IPOPT DEFAULT_MSG IPOPT_LIBRARIES IPOPT_INCLUDE_DIRS)
